<?php

/**
 * @file
 * Hooks provided by the Facebook Graph API.
 */

/**
 * Provide a label for tokens of a specific type.
 */
function facebook_graph_api_token_TOKEN_TYPE_label($token) {
  if ($account = user_load($token->type_id)) {
    return t('User @account', ['@account' => format_username($account)]);
  }
  return $token->type_id;
}
