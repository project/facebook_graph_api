CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers

INTRODUCTION
------------

The Facebook Graph API module provides Drupal integration for the Facebook Graph
API using the Facebook SDK for PHP.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/facebook_graph_api

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/facebook_graph_api

REQUIREMENTS
------------

This module requires the following:

 * PHP version 5.5 or higher
 * The Facebook SDK for PHP library (see installation instructions)
 * A Facebook user who will be used to call the Graph API
 * A Facebook App configured for use with the "Facebook Login" product (see
   configuration instructions)

INSTALLATION
------------

 * Install the module as you would normally install a contributed Drupal module.
   Visit: https://www.drupal.org/documentation/install/modules-themes/modules-7
   for further information.

 * Add the Facebook SDK for PHP library. If your Drupal site utilizes Composer
   Manager, enabling the Facebook Graph API module should load the library
   automatically.

 * If you are not managing your Drupal site using Composer Manager, you may
   manually install the library. Download the library from
   https://github.com/facebook/php-graph-sdk and place it into your site so that
   the library's src/Facebook/autoload.php file is located within your site at
   sites/all/libraries/facebook/graph-sdk/src/Facebook/autoload.php.


CONFIGURATION
-------------

 * This module requires a Facebook App. If needed, create a new App at
   https://developers.facebook.com/.

 * Add the Facebook Login product to the Facebook App if not already present.
   In the Facebook Login product settings, be sure to set the "Valid OAuth
   Redirect URIs" to the facebook-graph-api/oauth URL of your Drupal site. For
   example, https://awesomedrupalsite.com/facebook-graph-api/oauth.

 * Configure user permissions in Administration » People » Permissions:

   - Administer Facebook Graph API

     This permission is required to manage the Facebook "app" configuration and
     tokens utilized by this module.

 * Configure the App ID and App Secret for Facebook App to be used by the site
   in Administration » Configuration » Web services » Facebook Graph API »
   App Configuration. This information can be found in your dashboard on
   https://developers.facebook.com/.

 * Provision a "Site Master" token on Administration » Configuration »
   Web services » Facebook Graph API by clicking the "Add site token" link. This
   will direct you to Facebook where you will be prompted to login if needed,
   and to authorize the App to call the API on your Facebook user's behalf. If
   you're already logged in to Facebook and have previously authorized the App,
   you will be directed right back to the site with the access token.

MAINTAINERS
-----------

Current maintainers:
 * Will Long (kerasai) - https://www.drupal.org/u/kerasai

This project has been sponsored by:
 * Xeno Media, Inc.
   Visit https://www.xenomedia.com/ for more information.
