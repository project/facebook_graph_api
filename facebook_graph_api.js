/**
 * Initialize after FB has loaded.
 */
function FacebookGraphApiInit() {
  // Attach to the status change event.
  FB.Event.subscribe('auth.statusChange', function(response) {
    if (response.status == 'connected') {
      var url = location.protocol + '//' + location.host + Drupal.settings.basePath;
      url += 'facebook-graph-api/oauth/' + response.authResponse.accessToken;
      window.location.href = url;
    }
  });
}
