<?php

/**
 * @file
 * Facebook Graph API administrative page callbacks.
 */

/**
 * Page callback for token management page.
 */
function facebook_graph_api_admin_tokens_page() {
  $build['table'] = [
    '#theme' => 'table',
    '#header' => [t('Token'), t('Expiration'), t('Actions')],
    '#empty' => t('No tokens created.'),
  ];

  $query = db_select('facebook_graph_api_token', 't')->extend('PagerDefault');
  $query->fields('t', ['id'])->orderBy('id')->limit(50);

  $ids = $query->execute()->fetchCol();

  /** @var FacebookGraphApiToken[] $tokens */
  $tokens = entity_load('facebook_graph_api_token', $ids);
  $rows = [];
  foreach ($tokens as $token) {
    $row = [
      $token->label(),
      $token->expires !== NULL ? format_date($token->expires) : t('Never'),
    ];

    $links = [];
    $links[] = l(t('Info'), $token->getInfoUrl());
    $links[] = l(t('Delete'), $token->getDeleteUrl());
    $row[] = implode(' ', $links);

    $rows[] = $row;
  }
  $build['table']['#rows'] = $rows;

  $build['pager'] = ['#theme' => 'pager'];

  return $build;
}

/**
 * Page callback for token info page.
 *
 * @param \FacebookGraphApiToken $token
 *   The token.
 *
 * @return array
 *   The renderable page content.
 */
function facebook_graph_api_admin_token_info_page(FacebookGraphApiToken $token) {
  try {
    $info = print_r($token->getInfo(), TRUE);
  }
  catch (\Exception $e) {
    $info = $e->getMessage();
  }

  $build['status'] = [
    '#markup' => $info,
    '#prefix' => '<pre>',
    '#suffix' => '</pre>',
  ];

  $build['form'] = drupal_get_form('facebook_graph_api_admin_token_refresh_form', $token);
  return $build;
}

/**
 * Page callback for adding the site token.
 */
function facebook_graph_api_admin_site_token_add_page() {
  // Set token info.
  $handler = new FacebookGraphApiTokenCreationHandler();

  // If token not indicated, send to Facebook to authenticate.
  $params = drupal_get_query_parameters();
  if (empty($params['token'])) {
    $handler->setRedirect('admin/config/services/facebook-graph-api/tokens/site-master');
    facebook_graph_api_js_sdk(TRUE);
    return [
      'info' => [
        '#markup' => t('Continuing will redirect you to Facebook to generate a token with the currently-logged in Facebook user. To use a different Facebook user, first log out of Facebook then proceed.'),
        '#prefix' => '<div class="description">',
        '#suffix' => '</div>',
      ],
      'button' => [
        '#theme' => 'facebook_graph_api_login_button',
        '#weight' => 101,
      ],
    ];
  }

  if ($value = $handler->getToken()) {
    $token = new FacebookGraphApiToken([
      'type' => 'site',
      'type_id' => 'master',
      'token' => $value,
    ]);
    $token->save();
    drupal_set_message(t('Site token successfully set.'));
  }
  else {
    drupal_set_message(t('There was a problem obtaining the token.'), 'error');
  }

  $handler->clear();
  drupal_goto('admin/config/services/facebook-graph-api/tokens');
}
