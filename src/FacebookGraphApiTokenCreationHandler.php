<?php

/**
 * Class FacebookGraphApiTokenCreationHandler.
 */
class FacebookGraphApiTokenCreationHandler {

  /**
   * The handler data.
   *
   * @var array
   */
  protected $data;

  /**
   * FacebookGraphApiTokenCreationHandler constructor.
   */
  public function __construct() {
    if (empty($_SESSION['facebook_graph_api_token_create'])) {
      $_SESSION['facebook_graph_api_token_create'] = [];
    }

    $this->data =& $_SESSION['facebook_graph_api_token_create'];
  }

  /**
   * Clears the handler.
   */
  public function clear() {
    $this->data = [];
  }

  /**
   * Set the redirect.
   *
   * @param string $url
   *   The redirect URL.
   * @param array $options
   *   Optional, redirect options. Defaults to empty array.
   *
   * @see \drupal_goto()
   */
  public function setRedirect($url, array $options = []) {
    $this->data['redirect'] = ['url' => $url, 'options' => $options];
    return $this;
  }

  /**
   * Gets the redirect.
   *
   * @return array|false
   *   The redirect as an array containing "url" and "options", or FALSE if no
   *   redirect is set.
   */
  public function getRedirect() {
    return array_key_exists('redirect', $this->data) ? $this->data['redirect'] : FALSE;
  }

  /**
   * Set/clear the token.
   *
   * @param string $token
   *   Optional, set the token. If omitted, the token is cleared.
   */
  public function setToken($token = NULL) {
    if ($token === NULL) {
      unset($this->data['token']);
    }
    else {
      $this->data['token'] = $token;
    }
    return $this;
  }

  /**
   * Gets the token.
   *
   * @return string|null
   *   The token, or NULL if not set.
   */
  public function getToken() {
    return array_key_exists('token', $this->data) ? $this->data['token'] : NULL;
  }

  /**
   * Gets a value from the data.
   *
   * @return mixed|null
   *   The value, or NULL if not set.
   */
  public function get($name) {
    return array_key_exists($name, $this->data) ? $this->data[$name] : NULL;
  }

  /**
   * Set a value into the data.
   *
   * @param string $name
   *   The name of the data value to set.
   * @param mixed $value
   *   The value to set.
   *
   * @return $this
   */
  public function set($name, $value) {
    if ($value === NULL) {
      if (isset($this->data[$name])) {
        unset($this->data[$name]);
      }
    }
    else {
      $this->data[$name] = $value;
    }

    return $this;
  }

}
