<?php

/**
 * Class Token.
 */
class FacebookGraphApiToken extends \Entity {

  /**
   * Token constructor.
   *
   * @param array $values
   *   The entity values.
   */
  public function __construct(array $values = []) {
    parent::__construct($values, 'facebook_graph_api_token');
  }

  /**
   * {@inheritdoc}
   */
  public function label() {
    $func = "facebook_graph_api_token_{$this->type}_label";
    if (function_exists($func)) {
      return $func($this);
    }

    return t('@id (@type)', ['@type' => $this->type, '@id' => $this->type_id]);
  }

  /**
   * Gets the URL to the token delete page.
   *
   * @return string
   *   URL to the token delete page.
   */
  public function getDeleteUrl() {
    return url('admin/config/services/facebook-graph-api/token/' . $this->id . '/delete');
  }

  /**
   * Gets the URL to the token refresh page.
   *
   * @return string
   *   URL to the token refresh page.
   */
  public function getInfoUrl() {
    return url('admin/config/services/facebook-graph-api/token/' . $this->id . '/info');
  }

  /**
   * Gets info for the token.
   *
   * @return \Facebook\Authentication\AccessTokenMetadata
   *   The token info.
   */
  public function getInfo() {
    $oAuth2Client = $this->getFacebook()->getOAuth2Client();
    return $oAuth2Client->debugToken($this->token);
  }

  /**
   * Refreshes the token.
   */
  public function refresh() {

  }

  /**
   * Gets the Facebook SDK.
   *
   * @return \Facebook\Facebook
   *   The Facebook SDK.
   */
  protected function getFacebook() {
    return facebook_graph_api_get_facebook();
  }

}
