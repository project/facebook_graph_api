<?php

/**
 * @file
 * Facebook Graph API page callbacks.
 */

/**
 * Page callback for OAuth handling.
 */
function facebook_graph_api_oauth_page() {
  $fb = facebook_graph_api_get_facebook();
  $login = $fb->getRedirectLoginHelper();

  $handler = new FacebookGraphApiTokenCreationHandler();
  $accessToken = NULL;
  try {
    $accessToken = $login->getAccessToken();
  }
  catch (\Exception $e) {
    // No op.
  }

  if (!$accessToken) {
    $handler->setToken();
    facebook_graph_api_oauth_redirect();
    return t('There was a problem handling your oauth login.');
  }

  $handler->setToken($accessToken->getValue());
  facebook_graph_api_oauth_redirect();
  return t("You've successfully authenticated with the Facebook Graph API.");
}

/**
 * Page callback for handling tokens created outside of the PHP SDK.
 */
function facebook_graph_api_oauth_token_page($token) {
  $fb = facebook_graph_api_get_facebook();
  $handler = new FacebookGraphApiTokenCreationHandler();

  $oAuth2Client = $fb->getOAuth2Client();
  $tokenInfo = $oAuth2Client->debugToken($token);

  if (!$tokenInfo->getIsValid()) {
    $handler->setToken();
    facebook_graph_api_oauth_redirect();
    return t('There was a problem handling your oauth login.');
  }

  $handler->setToken($token);
  facebook_graph_api_oauth_redirect();
  return t("You've successfully authenticated with the Facebook Graph API.");
}
