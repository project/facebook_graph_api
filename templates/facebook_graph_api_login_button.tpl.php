<?php

/**
 * @file
 * Default theme implementation for Facebook for Javascript SDK login button.
 */
?>
<div class="fb-login-button"
     data-size="<?php print $size ?>"
     data-button-type="<?php print $button_type ?>"
     data-auto-logout-link="<?php print ($auto_logout_link ? 'TRUE' : 'FALSE') ?>"
     data-use-continue-as="<?php print ($use_continue_as ? 'TRUE' : 'FALSE') ?>"
     data-scope="<?php print $permissions; ?>"
     ></div>
