<?php

/**
 * @file
 * Facebook Graph API module code.
 */

use Facebook\Facebook;

define('FACEBOOK_GRAPH_API_AUTOLOADER', DRUPAL_ROOT . '/sites/all/libraries/facebook/graph-sdk/src/Facebook/autoload.php');
define('FACEBOOK_GRAPH_API_GRAPH_VERSION', 'v2.10');

/**
 * Implements hook_menu().
 */
function facebook_graph_api_menu() {
  $items['facebook-graph-api/oauth'] = [
    'title' => 'OAuth',
    'page callback' => 'facebook_graph_api_oauth_page',
    'access callback' => TRUE,
    'type' => MENU_CALLBACK,
    'file' => 'facebook_graph_api.pages.inc',
  ];

  $items['facebook-graph-api/oauth/%'] = [
    'title' => 'OAuth',
    'page callback' => 'facebook_graph_api_oauth_token_page',
    'page arguments' => [2],
    'access callback' => TRUE,
    'type' => MENU_CALLBACK,
    'file' => 'facebook_graph_api.pages.inc',
  ];

  $items['admin/config/services/facebook-graph-api'] = [
    'title' => 'Facebook Graph API',
    'description' => 'Manage Facebook Graph API.',
    'page callback' => 'system_admin_menu_block_page',
    'access arguments' => ['administer facebook_graph_api'],
    'type' => MENU_NORMAL_ITEM,
    'file path' => drupal_get_path('module', 'system'),
    'file' => 'system.admin.inc',
  ];

  $items['admin/config/services/facebook-graph-api/app'] = [
    'title' => 'App Configuration',
    'description' => 'Configure the Facebook app accessing the Graph API.',
    'page callback' => 'drupal_get_form',
    'page arguments' => ['facebook_graph_api_configure_form'],
    'access arguments' => ['administer facebook_graph_api'],
    'type' => MENU_NORMAL_ITEM,
  ];

  $items['admin/config/services/facebook-graph-api/tokens'] = [
    'title' => 'Tokens',
    'description' => 'Manage tokens used to access the Graph API.',
    'page callback' => 'facebook_graph_api_admin_tokens_page',
    'access arguments' => ['administer facebook_graph_api'],
    'type' => MENU_NORMAL_ITEM,
    'file' => 'facebook_graph_api.admin.inc',
  ];

  $items['admin/config/services/facebook-graph-api/tokens/site-master'] = [
    'title' => 'Add site token',
    'page callback' => 'facebook_graph_api_admin_site_token_add_page',
    'access callback' => 'facebook_graph_api_site_token_add_access',
    'access arguments' => ['administer facebook_graph_api'],
    'type' => MENU_LOCAL_ACTION,
    'file' => 'facebook_graph_api.admin.inc',
  ];

  $items['admin/config/services/facebook-graph-api/token/%facebook_graph_api_token/delete'] = [
    'title' => 'Delete Token',
    'page callback' => 'drupal_get_form',
    'page arguments' => ['facebook_graph_api_admin_token_delete_form', 5],
    'access arguments' => ['administer facebook_graph_api'],
    'type' => MENU_NORMAL_ITEM,
  ];

  $items['admin/config/services/facebook-graph-api/token/%facebook_graph_api_token/info'] = [
    'title' => 'Token Information',
    'page callback' => 'facebook_graph_api_admin_token_info_page',
    'page arguments' => [5],
    'access arguments' => ['administer facebook_graph_api'],
    'type' => MENU_NORMAL_ITEM,
    'file' => 'facebook_graph_api.admin.inc',
  ];

  return $items;
}

/**
 * Implements hook_permission().
 */
function facebook_graph_api_permission() {
  return [
    'administer facebook_graph_api' => [
      'title' => t('Administer Facebook Graph API'),
      'description' => t('Perform administration tasks for Facebook Graph API.'),
    ],
  ];
}

/**
 * Facebook Graph API configuration form.
 */
function facebook_graph_api_configure_form($form) {
  $form['facebook_graph_api_app_id'] = [
    '#title' => t('App ID'),
    '#type' => 'textfield',
    '#required' => TRUE,
    '#default_value' => variable_get('facebook_graph_api_app_id'),
  ];

  $form['facebook_graph_api_app_secret'] = [
    '#title' => t('App Secret'),
    '#type' => 'textfield',
    '#required' => TRUE,
    '#default_value' => variable_get('facebook_graph_api_app_secret'),
  ];

  return system_settings_form($form);
}

/**
 * Gets a Facebook object configured for use.
 *
 * @return \Facebook\Facebook
 *   The Facebook object.
 */
function facebook_graph_api_get_facebook() {
  // Attempt manual autoloader if needed.
  if (!class_exists('\Facebook\Facebook') && file_exists(FACEBOOK_GRAPH_API_AUTOLOADER)) {
    require_once FACEBOOK_GRAPH_API_AUTOLOADER;
  }

  $id = variable_get('facebook_graph_api_app_id');
  $secret = variable_get('facebook_graph_api_app_secret');

  $facebook = new Facebook([
    'app_id' => $id,
    'app_secret' => $secret,
    'default_graph_version' => FACEBOOK_GRAPH_API_GRAPH_VERSION,
  ]);
  $facebook->setDefaultAccessToken($facebook->getApp()->getAccessToken());

  return $facebook;
}

/**
 * Loads a token.
 *
 * @param int $id
 *   The token ID.
 *
 * @return \Entity|false
 *   The token entity, or FALSE if cannot be loaded.
 */
function facebook_graph_api_token_load($id) {
  return entity_load_single('facebook_graph_api_token', $id);
}

/**
 * Loads a token by type and type ID.
 *
 * @param string $type
 *   The token type.
 * @param string $type_id
 *   The token type ID.
 *
 * @return \Entity|false
 *   The token entity, or FLASE if cannot be loaded.
 */
function facebook_graph_api_token_load_by_type($type, $type_id) {
  $query = db_select('facebook_graph_api_token', 't')
    ->fields('t', ['id'])
    ->condition('type', $type)
    ->condition('type_id', $type_id);

  if (!$id = $query->execute()->fetchField()) {
    return FALSE;
  }

  return facebook_graph_api_token_load($id);
}

/**
 * Implements hook_entity_info().
 */
function facebook_graph_api_entity_info() {
  $entity['facebook_graph_api_token'] = [
    'label' => t('Facebook Graph API token'),
    'controller class' => 'EntityAPIController',
    'base table' => 'facebook_graph_api_token',
    'entity class' => 'FacebookGraphApiToken',
    'entity keys' => [
      'id' => 'id',
    ],
    'module' => 'facebook_graph_api',
  ];

  return $entity;
}

/**
 * Implements hook_entity_property_info().
 */
function facebook_graph_api_entity_property_info() {
  $properties = &$info['facebook_graph_api_token']['properties'];

  $properties['id'] = [
    'label' => t('The token ID'),
    'type' => 'integer',
    'description' => t('The token unique ID.'),
  ];

  $properties['type'] = [
    'label' => t('The token type'),
    'type' => 'string',
    'description' => t('The type the token is registered for.'),
  ];

  $properties['type_id'] = [
    'label' => t('The token type ID'),
    'type' => 'string',
    'description' => t('The token identifier within the type.'),
  ];

  $properties['token'] = [
    'label' => t('Value'),
    'type' => 'string',
    'description' => t('The token value.'),
  ];

  $properties['expires'] = [
    'label' => t('Expiration'),
    'type' => 'integer',
    'description' => t('Timestamp of the token expiration.'),
  ];

  return $info;
}

/**
 * Gets the OAuth login URL.
 *
 * @return string
 *   The OAuth login URL.
 */
function facebook_graph_api_oauth_url() {
  $fb = facebook_graph_api_get_facebook();
  $login = $fb->getRedirectLoginHelper();
  $options = [
    'absolute' => TRUE,
  ];
  return $login->getLoginUrl(url('facebook-graph-api/oauth', $options));
}

/**
 * Handles OAuth redirection.
 */
function facebook_graph_api_oauth_redirect() {
  $handler = new FacebookGraphApiTokenCreationHandler();
  $redirect = $handler->getRedirect();

  if ($redirect) {
    $redirect['options']['query']['token'] = 1;
    drupal_goto($redirect['url'], $redirect['options']);
  }
}

/**
 * Implements facebook_graph_api_token_TOKEN_TYPE_label().
 */
function facebook_graph_api_token_site_label($token) {
  return t('Site @id', ['@id' => $token->type_id]);
}

/**
 * Token delete form.
 */
function facebook_graph_api_admin_token_refresh_form($form, &$form_state, $token) {
  $form['#token_refresh'] = $token;
  $form['actions']['#type'] = 'actions';
  $form['actions']['submit'] = ['#type' => 'submit', '#value' => t('Refresh')];
  return $form;
}

/**
 * Submit handler for facebook_graph_api_admin_token_refresh_form.
 */
function facebook_graph_api_admin_token_refresh_form_submit($form, &$form_state) {
  /** @var \FacebookGraphApiToken $token */
  $token = $form['#token_refresh'];
  $token->refresh();
}

/**
 * Token delete form.
 */
function facebook_graph_api_admin_token_delete_form($form, &$form_state, $token) {
  $question = t('Are you sure you want to delete this token?');
  $path = 'admin/config/services/facebook-graph-api/tokens';

  /** @var \FacebookGraphApiToken $token */
  $form['#token_delete'] = $token;

  $markup = t('Token');
  $form['token'] = [
    '#markup' => '<strong>' . $markup . ':</strong> ' . check_plain($token->label()),
    '#prefix' => '<p>',
    '#suffix' => '</p>',
  ];

  return confirm_form($form, $question, $path);
}

/**
 * Submit handler for facebook_graph_api_admin_token_delete_form.
 */
function facebook_graph_api_admin_token_delete_form_submit($form, &$form_state) {
  /** @var \FacebookGraphApiToken $token */
  $token = $form['#token_delete'];
  $token->delete();
  drupal_set_message(t('The token has been deleted.'));
  $form_state['redirect'] = 'admin/config/services/facebook-graph-api/tokens';
}

/**
 * Determines if the site token is needed (does not exist).
 *
 * @return bool
 *   Indicates if the site token is needed.
 */
function facebook_graph_api_needs_site_token() {
  $token = facebook_graph_api_token_load_by_type('site', 'master');
  return empty($token);
}

/**
 * Access callback for site token add page.
 *
 * Requires user to have permission, and the site needs to have no existing
 * "site" type token with ID "master".
 *
 * @param string $permission
 *   The necessary permission.
 *
 * @return bool
 *   Indicates if the site token is needed.
 */
function facebook_graph_api_site_token_add_access($permission) {
  return facebook_graph_api_needs_site_token() && user_access($permission);
}

/**
 * Adds the Facebook SDK for JavaScript.
 *
 * @param bool|null $set
 *   Set the Facebook SDK for JavaScript to be included with the current page.
 *
 * @return bool
 *   Indicates if the Facebook SDK for JavaScript is to be included on the
 *   current page.
 */
function facebook_graph_api_js_sdk($set = NULL) {
  $include = &drupal_static(__FUNCTION__);
  if ($set === TRUE || $set === FALSE) {
    $include = $set;
  }
  return $include;
}

/**
 * Implements hook_page_build().
 */
function facebook_graph_api_page_build(&$page) {
  if (!facebook_graph_api_js_sdk()) {
    return;
  }

  if (!$app_id = variable_get('facebook_graph_api_app_id')) {
    watchdog(WATCHDOG_WARNING, 'Unable to utilize Facebook SDK for JavaScript: No Facebook APP ID set.');
    return;
  }

  $config = json_encode([
    'appId' => $app_id,
    'autoLogAppEvents' => TRUE,
    'xfbml' => TRUE,
    'version' => 'v3.2',
  ]);
  $js = 'FB.init(' . $config . ');';
  $js .= 'FacebookGraphApiInit();';
  $js = 'window.fbAsyncInit = function() { ' . $js . ' };';
  $js = '<script>' . $js . '</script>';
  $js .= '<script async defer src="https://connect.facebook.net/en_US/sdk.js"></script>';

  $page['page_top']['facebook_graph_api_js_sdk'] = [
    '#markup' => $js,
    '#weight' => -9999,
  ];
  $page['page_top']['facebook_graph_api_js_sdk']['#attached']['js'][] = [
    'type' => 'file',
    'data' => drupal_get_path('module', 'facebook_graph_api') . '/facebook_graph_api.js',
    'scope' => 'footer',
  ];
}

/**
 * Implements hook_theme().
 */
function facebook_graph_api_theme($existing, $type, $theme, $path) {
  $items['facebook_graph_api_login_button'] = [
    'variables' => [
      'size' => 'large',
      'button_type' => 'continue_with',
      'auto_logout_link' => FALSE,
      'use_continue_as' => TRUE,
      'permissions' => '',
    ],
    'template' => 'facebook_graph_api_login_button',
    'path' => $path . '/templates',
  ];

  return $items;
}
